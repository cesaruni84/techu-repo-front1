window.onload = init;

function init() {
    leerDeLocalStorage();
}

function guardarEnLocalStorage() {
    var id_ = document.getElementById("id").value; /* Referencia al input de clave */
    var nombre_ = document.getElementById("nombre").value; /* Referencia al input de valor */
    var apellidos_ = document.getElementById("apellidos").value; /* Referencia al input de valor */
    var ciudad_ = document.getElementById("ciudad").value; /* Referencia al input de valor */


    var objeto = {
        id: id_,
        nombre: nombre_,
        apellidos: apellidos_,
        ciudad: ciudad_
    };
    // localStorage.setItem("json", JSON.stringify(objeto));
    // console.log(JSON.stringify(objeto));
    sessionStorage.setItem(id_, JSON.stringify(objeto));
    init();

}


function borrarSessionStorage() {
    var idBorrar = document.getElementById("txtClaveBorrar").value; /* Referencia al input de clave */
    sessionStorage.removeItem(idBorrar);
    init();
}

function leerDeLocalStorage() {

    var table = document.getElementById("idTableClientes");
    table.innerHTML = "<tbody id=''idTableClientes''></tbody>";
    //console.log(table);
    // var table = document.createElement("idTableClientes");
    var totalItems = sessionStorage.length;
    for (var i = 1; i <= totalItems; i++) {
        var row = table.insertRow();
        var objeto = JSON.parse(sessionStorage.getItem(i));
        row.innerHTML = "<td> " + objeto.id + "</td>" +
            "<td>" + objeto.nombre + "</td>" +
            "<td>" + objeto.apellidos + "</td>" +
            "<td>" + objeto.ciudad + "</td>";
    }



}